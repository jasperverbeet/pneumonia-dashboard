<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Curl\Curl;

class ScanImage extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private function perform(string $path)
    {
        $curl = new Curl();
        $curl->post('http://pneumonia.api:5000/scan', array(
            'path' => $path,
        ));
        return $curl;
    }

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {   
        foreach ($models as $model) {

            $result = $this->perform($model->scan_image);
            if ($result->error) {
                return Action::message(
                    sprintf('Error %s', $result->error_code)
                );
            }
            
            \DB::table('scans')
                ->where('id', $model->id)
                ->update([
                    'diagnose' => $result->response,
                    'diagnosed_on' => \Carbon\Carbon::now()
                ]);
            
            $this->markAsFinished($model);

            return Action::message(
                sprintf('Successfully diagnosed %s', $model->user->name)
            );
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
