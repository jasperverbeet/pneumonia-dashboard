<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scan extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $casts = [
        'diagnosed_on' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
