# MediPortal

MediPortal is a prototyped portal that can be used by radiologists and doctors to automatically predict a diagnosis for an X-ray. Currently the algorithm only scans for fneumonia in the chest area (93%) but future versions will also support the detection of lesions in the chest and brain area. Support for the DICOM format and detection of Alzheimer's is also on the roadmap.

## Video

[![Pneumonia dashboard preview](https://i.imgur.com/DUP7kWf.png)](https://vimeo.com/312253424 "Dashboard preview - Klik om te bekijken!")

## Photos

![photo](images/3.png)
![photo](images/5.png)
![photo](images/4.png)