from sklearn.externals import joblib
from sklearn.preprocessing import StandardScaler
from scipy.cluster.vq import vq
from PIL import Image
from io import BytesIO
import numpy as np
import base64
import logging
import cv2

class Classify:
    def __init__(self, app):
        _joblib_model = joblib.load('/model/model.pkl')
        
        self.classifier = _joblib_model[0]
        self.scaler = _joblib_model[1]
        self.num_of_clusters = _joblib_model[2]
        self.codebook = _joblib_model[3]
        self.sift = cv2.xfeatures2d.SIFT_create()
        self.app = app

    def predict(self, filename):
        self.app.logger.info('Getting image ... ')
        image = cv2.imread(filename)
        self.app.logger.info('Creating descriptors ...')
        descriptors = self._create_descriptors(image)
        self.app.logger.info('Creating histogram ...')
        histogram = self._generate_histogram(descriptors)
        # self.app.logger.info(histogram)
        self.app.logger.info('Normalizing histogram ...')
        normalized = self._normalize(histogram)
        self.app.logger.info('Classifying histogram ...')
        return self.classifier.predict(normalized)
        
    def _generate_histogram(self, descriptors):
        histogram_set = np.zeros((1, self.num_of_clusters), "float32")
        words, distance = vq(descriptors, self.codebook)
        for word in words:
            histogram_set[0][word] += 1

        return histogram_set

    def _normalize(self, histogram):
        return self.scaler.transform(histogram)

    def _create_descriptors(self, image):
        kp, des = self.sift.detectAndCompute(image, None)
        return des