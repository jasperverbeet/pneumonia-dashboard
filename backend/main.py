import os
from flask import Flask, request, redirect, url_for, render_template
from werkzeug.utils import secure_filename
from classify import Classify

UPLOAD_FOLDER = '/uploads'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

app = Flask(__name__,template_folder='/app/templates')
classifier = Classify(app)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
def index():
    return 'Alive'

@app.route('/scan', methods=['POST'])
def upload_file():
    app.logger.info(request.form)

    filename = os.path.join(UPLOAD_FOLDER, secure_filename(request.form['path']))

    app.logger.info(filename)

    result = classifier.predict(filename)

    app.logger.info('Predicted: {}'.format(str(result)))
    return 'Pneumonia' if result[0] == 1 else 'Normal'

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')