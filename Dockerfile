FROM jasperverbeet/datascience_python

COPY ./requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

RUN mkdir /model && wget https://i342550.hera.fhict.nl/model_v5.pkl -O /model/model.pkl

COPY ./backend /app
WORKDIR /app

RUN mkdir /uploads

ENTRYPOINT [ "python" ]
CMD [ "main.py" ]